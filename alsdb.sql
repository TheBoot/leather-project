--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10
-- Dumped by pg_dump version 11.2

-- Started on 2020-01-21 00:02:52

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 16403)
-- Name: t_category; Type: TABLE; Schema: public; Owner: strapkul
--

CREATE TABLE public.t_category (
    id_category character varying(12) NOT NULL,
    name character varying(32) NOT NULL,
    url_param character varying(24)
);


ALTER TABLE public.t_category OWNER TO strapkul;

--
-- TOC entry 199 (class 1259 OID 16422)
-- Name: t_images; Type: TABLE; Schema: public; Owner: strapkul
--

CREATE TABLE public.t_images (
    name character varying(256) NOT NULL,
    location character varying(128) NOT NULL,
    created_by character varying(32),
    modified_by character varying(32),
    created_date timestamp with time zone,
    modified_date time with time zone,
    id_product character varying(32) NOT NULL,
    id_image character varying(32) NOT NULL
);


ALTER TABLE public.t_images OWNER TO strapkul;

--
-- TOC entry 198 (class 1259 OID 16406)
-- Name: t_product; Type: TABLE; Schema: public; Owner: strapkul
--

CREATE TABLE public.t_product (
    name character varying(128) NOT NULL,
    description character varying(1024),
    created_date timestamp(6) with time zone,
    modified_date time(6) with time zone,
    created_by character varying(32),
    modified_by character varying(32),
    id_category character varying(12),
    link_product character varying(256),
    id_product character varying(32) NOT NULL,
    size character varying(32),
    price character varying(32)
);


ALTER TABLE public.t_product OWNER TO strapkul;

--
-- TOC entry 196 (class 1259 OID 16398)
-- Name: t_user; Type: TABLE; Schema: public; Owner: strapkul
--

CREATE TABLE public.t_user (
    id_user character varying(32) NOT NULL,
    password character varying(64) NOT NULL,
    name character varying(256),
    role character varying(12) NOT NULL
);


ALTER TABLE public.t_user OWNER TO strapkul;

--
-- TOC entry 2816 (class 0 OID 16403)
-- Dependencies: 197
-- Data for Name: t_category; Type: TABLE DATA; Schema: public; Owner: strapkul
--

COPY public.t_category (id_category, name, url_param) FROM stdin;
C001	Vintage	Vintage
C002	Abstract	Abstract
C003	Sunburst	Sunburst
\.


--
-- TOC entry 2818 (class 0 OID 16422)
-- Dependencies: 199
-- Data for Name: t_images; Type: TABLE DATA; Schema: public; Owner: strapkul
--

COPY public.t_images (name, location, created_by, modified_by, created_date, modified_date, id_product, id_image) FROM stdin;
imgStraps_1579446065576_IMG_0307_toped.jpg	/public/img/imgStraps_1579446065576_IMG_0307_toped.jpg	izzul	\N	2020-01-19 22:01:05.824526+07	\N	znAJ8pNuGeIb7W3Q	uR7bgrOorm0fUG4bZL5u0kpavuhyaaiD
imgStraps_1579446065626_IMG_0312_toped.jpg	/public/img/imgStraps_1579446065626_IMG_0312_toped.jpg	izzul	\N	2020-01-19 22:01:05.824666+07	\N	znAJ8pNuGeIb7W3Q	iZvNrBIhI4ks2Uen9RCB7US4rTUjvqsy
imgStraps_1579446065650_IMG_0315_toped.jpg	/public/img/imgStraps_1579446065650_IMG_0315_toped.jpg	izzul	\N	2020-01-19 22:01:05.909819+07	\N	znAJ8pNuGeIb7W3Q	AYNY4NoTztlD2PquG6YRLNW641nwCriI
imgStraps_1579446065644_IMG_0313_toped.jpg	/public/img/imgStraps_1579446065644_IMG_0313_toped.jpg	izzul	\N	2020-01-19 22:01:05.914349+07	\N	znAJ8pNuGeIb7W3Q	5gYtXHBrrWe4houRF4nwjLbyus5Fhksu
imgStraps_1579446065656_IMG_0318_toped.jpg	/public/img/imgStraps_1579446065656_IMG_0318_toped.jpg	izzul	\N	2020-01-19 22:01:05.919017+07	\N	znAJ8pNuGeIb7W3Q	eIW4Dfh4w4SPkKbf3Br4LHKpYZ5ME9cE
imgStraps_1579447574473_IMG_0319_toped.jpg	/public/img/imgStraps_1579447574473_IMG_0319_toped.jpg	izzul	\N	2020-01-19 22:26:14.646699+07	\N	OfizGkElD3l3guME	8EJ7vkSXK17FxS6QSsoRLZW4aZNWsoiB
imgStraps_1579447574488_IMG_0321_toped.jpg	/public/img/imgStraps_1579447574488_IMG_0321_toped.jpg	izzul	\N	2020-01-19 22:26:14.646889+07	\N	OfizGkElD3l3guME	LgwCBJsa1RldBmItVMuXpievoVX6Nv2O
imgStraps_1579447574511_IMG_0323_toped.jpg	/public/img/imgStraps_1579447574511_IMG_0323_toped.jpg	izzul	\N	2020-01-19 22:26:14.722519+07	\N	OfizGkElD3l3guME	l268wGBcoMqSr0KEayeGoYqLlJFhJsHJ
imgStraps_1579447574524_IMG_0324_toped.jpg	/public/img/imgStraps_1579447574524_IMG_0324_toped.jpg	izzul	\N	2020-01-19 22:26:14.723874+07	\N	OfizGkElD3l3guME	nVlTwfGL8TOgC8we9HINIXomKm0c0M2I
imgStraps_1579447574529_IMG_0325_toped.jpg	/public/img/imgStraps_1579447574529_IMG_0325_toped.jpg	izzul	\N	2020-01-19 22:26:14.725422+07	\N	OfizGkElD3l3guME	l5DrrUO6BSSHPuO9zPZhNW65rG83LC0t
imgStraps_1579447687356_IMG_0328_toped.jpg	/public/img/imgStraps_1579447687356_IMG_0328_toped.jpg	izzul	\N	2020-01-19 22:28:07.46639+07	\N	NRh56WMIC592A6Z5	1IaBERNahScDR9rNF1EHrPHD8Tpfx7rF
imgStraps_1579447687378_IMG_0329_toped.jpg	/public/img/imgStraps_1579447687378_IMG_0329_toped.jpg	izzul	\N	2020-01-19 22:28:07.618577+07	\N	NRh56WMIC592A6Z5	qSPNyFagrntlPslVrNKDcWSrSNj9gsiw
imgStraps_1579447687384_IMG_0330_toped.jpg	/public/img/imgStraps_1579447687384_IMG_0330_toped.jpg	izzul	\N	2020-01-19 22:28:07.624806+07	\N	NRh56WMIC592A6Z5	LVhuxQ3dMEOCpbp0vxEy3lBOcIPhDO69
imgStraps_1579447687390_IMG_0332_toped.jpg	/public/img/imgStraps_1579447687390_IMG_0332_toped.jpg	izzul	\N	2020-01-19 22:28:07.628645+07	\N	NRh56WMIC592A6Z5	Hxw8X1GgRE4byuViPDkjfBmoPnGvkui0
imgStraps_1579447940152_IMG_0248_toped.jpg	/public/img/imgStraps_1579447940152_IMG_0248_toped.jpg	izzul	\N	2020-01-19 22:32:20.468148+07	\N	AONilhLb2StQFwqC	PtMQvVYT8FZouKJ49C55Qyw9kWEf7WVO
imgStraps_1579447940185_IMG_0250_toped.jpg	/public/img/imgStraps_1579447940185_IMG_0250_toped.jpg	izzul	\N	2020-01-19 22:32:20.468272+07	\N	AONilhLb2StQFwqC	nz9wizCu1jZqiy9PFfxD0NIgzwycnyEB
imgStraps_1579447940308_IMG_0252_toped.jpg	/public/img/imgStraps_1579447940308_IMG_0252_toped.jpg	izzul	\N	2020-01-19 22:32:20.545854+07	\N	AONilhLb2StQFwqC	3FussGawgsy942xUzcN1jNgGfL61SwcD
imgStraps_1579447940277_IMG_0251_toped.jpg	/public/img/imgStraps_1579447940277_IMG_0251_toped.jpg	izzul	\N	2020-01-19 22:32:20.546456+07	\N	AONilhLb2StQFwqC	00EzVFOjdU0diSCzoVd3VbqTn3hCzXZS
imgStraps_1579447940317_IMG_0254_toped.jpg	/public/img/imgStraps_1579447940317_IMG_0254_toped.jpg	izzul	\N	2020-01-19 22:32:20.549748+07	\N	AONilhLb2StQFwqC	06kyZJsDzmFepliM8GPqJAQaFfJ7PUaS
imgStraps_1579448090880_IMG_0230_toped.jpg	/public/img/imgStraps_1579448090880_IMG_0230_toped.jpg	izzul	\N	2020-01-19 22:34:51.163611+07	\N	s6Ov9tbsuCZCjegi	V1RJkxwGcetr7nlwFc1kQHGSiXILqQ8r
imgStraps_1579448090948_IMG_0234_toped.jpg	/public/img/imgStraps_1579448090948_IMG_0234_toped.jpg	izzul	\N	2020-01-19 22:34:51.290509+07	\N	s6Ov9tbsuCZCjegi	0osGlq8vm5mgrPLBoIO6q5p6R6oHhSFT
imgStraps_1579448091000_IMG_0244_toped.jpg	/public/img/imgStraps_1579448091000_IMG_0244_toped.jpg	izzul	\N	2020-01-19 22:34:51.290945+07	\N	s6Ov9tbsuCZCjegi	kUWkTdxaZx6ibJTHn43MBnMImv4zhlGB
imgStraps_1579448090977_IMG_0241_toped.jpg	/public/img/imgStraps_1579448090977_IMG_0241_toped.jpg	izzul	\N	2020-01-19 22:34:51.291357+07	\N	s6Ov9tbsuCZCjegi	gXZhiMzjasV4lrldiSkUw6PKYzi4l7BK
imgStraps_1579448091040_IMG_0245_toped.jpg	/public/img/imgStraps_1579448091040_IMG_0245_toped.jpg	izzul	\N	2020-01-19 22:34:51.29177+07	\N	s6Ov9tbsuCZCjegi	mCuM83y4o6b0DW9RLg24JMhEtHvrj9zi
imgStraps_1579448345714_IMG_0335_toped.jpg	/public/img/imgStraps_1579448345714_IMG_0335_toped.jpg	izzul	\N	2020-01-19 22:39:05.921624+07	\N	etCrFRfiKrauDrmG	2LcuGyS193Jiy1xXhLxqcmJ6oOEUpPJl
imgStraps_1579448345721_IMG_0338_toped.jpg	/public/img/imgStraps_1579448345721_IMG_0338_toped.jpg	izzul	\N	2020-01-19 22:39:05.921884+07	\N	etCrFRfiKrauDrmG	IowsrLr5RDnOMoOh1KsZwmxQC0m2DT0U
imgStraps_1579448345731_IMG_0342_toped.jpg	/public/img/imgStraps_1579448345731_IMG_0342_toped.jpg	izzul	\N	2020-01-19 22:39:05.994821+07	\N	etCrFRfiKrauDrmG	AyTfEadb1920GLoqMcQ0FIoXk3LENjWv
imgStraps_1579448345750_IMG_0348_toped.jpg	/public/img/imgStraps_1579448345750_IMG_0348_toped.jpg	izzul	\N	2020-01-19 22:39:05.995377+07	\N	etCrFRfiKrauDrmG	6tbtyXAz0nqVWAWjyUavxbeu2jp9aJK6
imgStraps_1579448530072_IMG_0353_toped.jpg	/public/img/imgStraps_1579448530072_IMG_0353_toped.jpg	izzul	\N	2020-01-19 22:42:10.258684+07	\N	M42Uq1HcvCpUZ9zf	dUFICs5dnR29z7JzfPNRLP1sxF1usaqh
imgStraps_1579448530078_IMG_0355_toped.jpg	/public/img/imgStraps_1579448530078_IMG_0355_toped.jpg	izzul	\N	2020-01-19 22:42:10.258853+07	\N	M42Uq1HcvCpUZ9zf	BJAEOh60ZU7bYwWuyiBdlVuDoLcx4dCC
imgStraps_1579448530102_IMG_0358_toped.jpg	/public/img/imgStraps_1579448530102_IMG_0358_toped.jpg	izzul	\N	2020-01-19 22:42:10.33873+07	\N	M42Uq1HcvCpUZ9zf	KEvIYFuyeJxGsSk2OfwCdgk8sy0cvtQR
imgStraps_1579448530096_IMG_0357_toped.jpg	/public/img/imgStraps_1579448530096_IMG_0357_toped.jpg	izzul	\N	2020-01-19 22:42:10.342174+07	\N	M42Uq1HcvCpUZ9zf	4dhhGzpvoa0U5aNjl3PIS5hHSx9h7uqS
imgStraps_1579448530109_IMG_0359_toped.jpg	/public/img/imgStraps_1579448530109_IMG_0359_toped.jpg	izzul	\N	2020-01-19 22:42:10.351128+07	\N	M42Uq1HcvCpUZ9zf	g2BBKxjCAoETP6hUI2LfV3PY4qAfPYo4
imgStraps_1579448583805_IMG_0361_toped.jpg	/public/img/imgStraps_1579448583805_IMG_0361_toped.jpg	izzul	\N	2020-01-19 22:43:03.913998+07	\N	sOlLTeJ8iBjOvECC	dDLz7z9KzrjQL9vDMG5lavOWvVbLHlYU
imgStraps_1579448583812_IMG_0362_toped.jpg	/public/img/imgStraps_1579448583812_IMG_0362_toped.jpg	izzul	\N	2020-01-19 22:43:04.031515+07	\N	sOlLTeJ8iBjOvECC	GaMjxDLSpXdn9cugzhnJHa93BBzU0XYF
imgStraps_1579448583820_IMG_0364_toped.jpg	/public/img/imgStraps_1579448583820_IMG_0364_toped.jpg	izzul	\N	2020-01-19 22:43:04.046121+07	\N	sOlLTeJ8iBjOvECC	jKXM4HVe0MmLPJId4NlH3MsBeOajc3Ji
imgStraps_1579448583829_IMG_0365_toped.jpg	/public/img/imgStraps_1579448583829_IMG_0365_toped.jpg	izzul	\N	2020-01-19 22:43:04.048477+07	\N	sOlLTeJ8iBjOvECC	VvAy8R0E47wGhi12i9LeTycZv7q32VHe
imgStraps_1579448583838_IMG_0367_toped.jpg	/public/img/imgStraps_1579448583838_IMG_0367_toped.jpg	izzul	\N	2020-01-19 22:43:04.053844+07	\N	sOlLTeJ8iBjOvECC	xGJ9EodVfnf9pCEC2SGC4epmR9SmHVdT
imgStraps_1579448651587_IMG_0376_toped.jpg	/public/img/imgStraps_1579448651587_IMG_0376_toped.jpg	izzul	\N	2020-01-19 22:44:11.752987+07	\N	KfBpgWo67AWRI0AO	8umgv1Sy3mZOI5ugyInUyDMCls3Di5O9
imgStraps_1579448651595_IMG_0377_toped.jpg	/public/img/imgStraps_1579448651595_IMG_0377_toped.jpg	izzul	\N	2020-01-19 22:44:11.753239+07	\N	KfBpgWo67AWRI0AO	WabzFP5X5RBTc7hj07ZzSi7CLi3FopjU
imgStraps_1579448651603_IMG_0379_toped.jpg	/public/img/imgStraps_1579448651603_IMG_0379_toped.jpg	izzul	\N	2020-01-19 22:44:11.840193+07	\N	KfBpgWo67AWRI0AO	TLBKFWTTnuGx6z5jm5iJb1cQsiBpvyCL
imgStraps_1579448651613_IMG_0381_toped.jpg	/public/img/imgStraps_1579448651613_IMG_0381_toped.jpg	izzul	\N	2020-01-19 22:44:11.846256+07	\N	KfBpgWo67AWRI0AO	XGzkyDlgjZ5N8ljDZN794RBfR3LQ5U8n
imgStraps_1579449020942_IMG_0369_toped.jpg	/public/img/imgStraps_1579449020942_IMG_0369_toped.jpg	izzul	\N	2020-01-19 22:50:21.062461+07	\N	JLvYcdrT8anhEkP2	UZFECeL7YESE12ld91UymWCBmDm5FpQV
imgStraps_1579449020952_IMG_0370_toped.jpg	/public/img/imgStraps_1579449020952_IMG_0370_toped.jpg	izzul	\N	2020-01-19 22:50:21.236185+07	\N	JLvYcdrT8anhEkP2	ZfnRAdS3HQIAxrUGNPJ2qyEaDItcb3ar
imgStraps_1579449020973_IMG_0373_toped.jpg	/public/img/imgStraps_1579449020973_IMG_0373_toped.jpg	izzul	\N	2020-01-19 22:50:21.325446+07	\N	JLvYcdrT8anhEkP2	jmG3BawYgkecvOCCMPstCM14SpMnoBSw
imgStraps_1579449020977_IMG_0375_toped.jpg	/public/img/imgStraps_1579449020977_IMG_0375_toped.jpg	izzul	\N	2020-01-19 22:50:21.326465+07	\N	JLvYcdrT8anhEkP2	83vW6jLQR4PgvcEAzdk0W2iO6aBcVSpy
imgStraps_1579449081765_IMG_0382_toped.jpg	/public/img/imgStraps_1579449081765_IMG_0382_toped.jpg	izzul	\N	2020-01-19 22:51:21.991797+07	\N	lHzobzJeT0P06UH2	4NNYJ6asbn4fzfeb98GZVzUUpDDW2aXw
imgStraps_1579449081772_IMG_0385_toped.jpg	/public/img/imgStraps_1579449081772_IMG_0385_toped.jpg	izzul	\N	2020-01-19 22:51:21.991965+07	\N	lHzobzJeT0P06UH2	FE7faC7GBQcg58PQvrSZ1SsgyKGY2aWR
imgStraps_1579449081782_IMG_0387_toped.jpg	/public/img/imgStraps_1579449081782_IMG_0387_toped.jpg	izzul	\N	2020-01-19 22:51:22.084843+07	\N	lHzobzJeT0P06UH2	mIj6PA6xCYPG1DVs68ZIJb8yGN0fSlCF
imgStraps_1579449081789_IMG_0388_toped.jpg	/public/img/imgStraps_1579449081789_IMG_0388_toped.jpg	izzul	\N	2020-01-19 22:51:22.08798+07	\N	lHzobzJeT0P06UH2	EL5aepZSKMJPWsWMkiu1NSqQVW7Cec63
imgStraps_1579449081800_IMG_0389_toped.jpg	/public/img/imgStraps_1579449081800_IMG_0389_toped.jpg	izzul	\N	2020-01-19 22:51:22.088615+07	\N	lHzobzJeT0P06UH2	JaWTceiKdmU6ZdN1iUiMpSGxUi6YTYTX
imgStraps_1579449183516_IMG_0391_toped.jpg	/public/img/imgStraps_1579449183516_IMG_0391_toped.jpg	izzul	\N	2020-01-19 22:53:03.614266+07	\N	18pWb3FOk5TGjCq5	LLYCsUZEuoTyn7u1x88jiI9u8rchtd73
imgStraps_1579449183532_IMG_0394_toped.jpg	/public/img/imgStraps_1579449183532_IMG_0394_toped.jpg	izzul	\N	2020-01-19 22:53:03.754332+07	\N	18pWb3FOk5TGjCq5	LU4LHMfDSAWuqRftuRjKxpHZDp7vdvSw
imgStraps_1579449183539_IMG_0396_toped.jpg	/public/img/imgStraps_1579449183539_IMG_0396_toped.jpg	izzul	\N	2020-01-19 22:53:03.760023+07	\N	18pWb3FOk5TGjCq5	PouTy7iD6EviKTSJbRUkOn5IayGkwtp2
imgStraps_1579449183548_IMG_0400_toped.jpg	/public/img/imgStraps_1579449183548_IMG_0400_toped.jpg	izzul	\N	2020-01-19 22:53:03.770817+07	\N	18pWb3FOk5TGjCq5	MLtBwM4O8mcuEpUs4CuCAE0Elfzptoru
\.


--
-- TOC entry 2817 (class 0 OID 16406)
-- Dependencies: 198
-- Data for Name: t_product; Type: TABLE DATA; Schema: public; Owner: strapkul
--

COPY public.t_product (name, description, created_date, modified_date, created_by, modified_by, id_category, link_product, id_product, size, price) FROM stdin;
De Dust Full Stitch	Material :\r\nLeather Cow ( Natural Nabati )\r\n\r\nDimension :\r\nLong Part : 125 mm\r\nShort Part : 75 mm\r\nThickness : 2.5 - 3 mm\r\n\r\nSize :\r\n- 18 mm\r\n- 20 mm\r\n\r\nSpecification:\r\n- 5 Buckle Hole\r\n- 2 Keeper\r\n- Full Stitch\r\n- English Point\r\n\r\n- Buckle\r\n\r\nbeberapa merk jam memiliki ukuran yang berbeda silahkan diskusi dengan kami melalu fitur form/email	2020-01-19 22:01:05.736311+07	\N	izzul	\N	C001	http://strapkulit.com/product/Vintage/znAJ8pNuGeIb7W3Q	znAJ8pNuGeIb7W3Q	18/20 mm	200000
De Dust Low Stitch Black	Material :\r\nLeather Cow ( Natural Nabati )\r\n\r\nDimension :\r\nLong Part : 125 mm\r\nShort Part : 75 mm\r\nThickness : 2.5 - 3 mm\r\n\r\nSize :\r\n- 20 mm\r\n- 22 mm\r\n- 24 mm\r\n\r\nSpecification:\r\n- 5 Buckle Hole\r\n- 2 Keeper\r\n- Low Stitch\r\n- English Point\r\n\r\nbeberapa merk jam memiliki ukuran yang berbeda silahkan diskusi dengan kami melalu fitur form/email	2020-01-19 22:26:14.591771+07	\N	izzul	\N	C001	http://strapkulit.com/product/Vintage/OfizGkElD3l3guME	OfizGkElD3l3guME	20/22/24 mm	170000
De Dust Low Stitch Cream	Material :\r\nLeather Cow ( Natural Nabati )\r\n\r\nDimension :\r\nLong Part : 125 mm\r\nShort Part : 75 mm\r\nThickness : 2.5 - 3 mm\r\n\r\nSize :\r\n- 20 mm\r\n- 22 mm\r\n\r\nSpecification:\r\n- 5 Buckle Hole\r\n- 2 Keeper\r\n- Low Stitch\r\n- English Point\r\n\r\nbeberapa merk jam memiliki ukuran yang berbeda silahkan diskusi dengan kami melalu fitur form/email	2020-01-19 22:28:07.454406+07	\N	izzul	\N	C001	http://strapkulit.com/product/Vintage/NRh56WMIC592A6Z5	NRh56WMIC592A6Z5	20/22 mm	170000
Light Manggo Low Stitch	Material :\r\nLeather Cow ( Natural Nabati )\r\n\r\nDimension :\r\nLong Part : 125 mm\r\nShort Part : 75 mm\r\nThickness : 2.5 - 3 mm\r\nSize : 22 to 20 mm\r\n\r\nSpecification:\r\n- 5 Buckle Hole\r\n- 2 Keeper\r\n- Low Stitch\r\n- Round Point\r\n\r\nbeberapa merk jam memiliki ukuran yang berbeda silahkan diskusi dengan kami melalu fitur form/email	2020-01-19 22:32:20.438605+07	\N	izzul	\N	C001	http://strapkulit.com/product/Vintage/AONilhLb2StQFwqC	AONilhLb2StQFwqC	22 to 20 mm	160000
Light Manggo Full Stitch	Material :\r\nLeather Cow ( Natural Nabati )\r\n\r\nDimension :\r\nLong Part : 125 mm\r\nShort Part : 75 mm\r\nThickness : 2.5 - 3 mm\r\nSize : 22 to 20 mm\r\n\r\nSpecification:\r\n- 5 Buckle Hole\r\n- 2 Keeper\r\n- Full Stitch\r\n- English Point\r\n\r\n\r\nbeberapa merk jam memiliki ukuran yang berbeda silahkan diskusi dengan kami melalu fitur form/email	2020-01-19 22:34:51.14614+07	\N	izzul	\N	C001	http://strapkulit.com/product/Vintage/s6Ov9tbsuCZCjegi	s6Ov9tbsuCZCjegi	22 to 20 mm	180000
Long Beach Low Stitch	Material :\r\nLeather Cow ( Natural Nabati )\r\n\r\nDimension :\r\nLong Part : 125 mm\r\nShort Part : 75 mm\r\nThickness : 2.5 - 3 mm\r\nSize : 22 to 20 mm\r\n\r\nSpecification:\r\n- 5 Buckle Hole\r\n- 2 Keeper\r\n- Low Stitch\r\n- English Point\r\n\r\nbeberapa merk jam memiliki ukuran yang berbeda silahkan diskusi dengan kami melalu fitur form/email	2020-01-19 22:39:05.902804+07	\N	izzul	\N	C003	http://strapkulit.com/product/Sunburst/etCrFRfiKrauDrmG	etCrFRfiKrauDrmG	22 to 20 mm	180000
Bamboo Forest #1 Low Stitch	\r\nMaterial :\r\nLeather Cow ( Natural Nabati )\r\n\r\nDimension :\r\nLong Part : 125 mm\r\nShort Part : 75 mm\r\nThickness : 2.5 - 3 mm\r\nSize : 18 mm\r\n\r\nSpecification:\r\n- 5 Buckle Hole\r\n- 2 Keeper\r\n- Low Stitch\r\n- English Point\r\n\r\nbeberapa merk jam memiliki ukuran yang berbeda silahkan diskusi dengan kami melalu fitur form/email	2020-01-19 22:42:10.189426+07	\N	izzul	\N	C002	http://strapkulit.com/product/Abstract/M42Uq1HcvCpUZ9zf	M42Uq1HcvCpUZ9zf	18 mm	180000
Bamboo Forest #2 Low Stitch	\r\nMaterial :\r\nLeather Cow ( Natural Nabati )\r\n\r\nDimension :\r\nLong Part : 125 mm\r\nShort Part : 75 mm\r\nThickness : 2.5 - 3 mm\r\nSize : 20 mm\r\n\r\nSpecification:\r\n- 5 Buckle Hole\r\n- 2 Keeper\r\n- Low Stitch\r\n- English Point\r\n\r\nbeberapa merk jam memiliki ukuran yang berbeda silahkan diskusi dengan kami melalu fitur form/email	2020-01-19 22:43:03.899841+07	\N	izzul	\N	C002	http://strapkulit.com/product/Abstract/sOlLTeJ8iBjOvECC	sOlLTeJ8iBjOvECC	20 mm	180000
Bamboo Forest #3 Low Stitch	\r\nMaterial :\r\nLeather Cow ( Natural Nabati )\r\n\r\nDimension :\r\nLong Part : 125 mm\r\nShort Part : 75 mm\r\nThickness : 2.5 - 3 mm\r\nSize : 20 mm\r\n\r\nSpecification:\r\n- 5 Buckle Hole\r\n- 2 Keeper\r\n- Low Stitch\r\n- English Point\r\n\r\nbeberapa merk jam memiliki ukuran yang berbeda silahkan diskusi dengan kami melalu fitur form/email	2020-01-19 22:44:11.735083+07	\N	izzul	\N	C002	http://strapkulit.com/product/Abstract/KfBpgWo67AWRI0AO	KfBpgWo67AWRI0AO	20 mm	180000
Bamboo Forest #4 Low Stitch	Material :\r\nLeather Cow ( Natural Nabati )\r\n\r\nDimension :\r\nLong Part : 125 mm\r\nShort Part : 75 mm\r\nThickness : 2.5 - 3 mm\r\nSize : 22 mm\r\n\r\nSpecification:\r\n- 5 Buckle Hole\r\n- 2 Keeper\r\n- Low Stitch\r\n- English Point\r\n\r\nBuckle x 1\r\n\r\nbeberapa merk jam memiliki ukuran yang berbeda silahkan diskusi dengan kami melalu fitur form/email	2020-01-19 22:50:21.042958+07	\N	izzul	\N	C002	http://strapkulit.com/product/Abstract/JLvYcdrT8anhEkP2	JLvYcdrT8anhEkP2	22 mm	200000
Bamboo Forest #5 Low Stitch	Material :\r\nLeather Cow ( Natural Nabati )\r\n\r\nDimension :\r\nLong Part : 125 mm\r\nShort Part : 75 mm\r\nThickness : 2.5 - 3 mm\r\nSize : 20 mm\r\n\r\nSpecification:\r\n- 5 Buckle Hole\r\n- 2 Keeper\r\n- Low Stitch\r\n- English Point\r\n\r\nBuckle x 1\r\n\r\nbeberapa merk jam memiliki ukuran yang berbeda silahkan diskusi dengan kami melalu fitur form/email	2020-01-19 22:51:21.977089+07	\N	izzul	\N	C002	http://strapkulit.com/product/Abstract/lHzobzJeT0P06UH2	lHzobzJeT0P06UH2	20 mm	200000
Bamboo Forest #6 Low Stitch	\r\nMaterial :\r\nLeather Cow ( Natural Nabati )\r\n\r\nDimension :\r\nLong Part : 125 mm\r\nShort Part : 75 mm\r\nThickness : 2.5 - 3 mm\r\nSize : 18 mm\r\n\r\nSpecification:\r\n- 5 Buckle Hole\r\n- 2 Keeper\r\n- Low Stitch\r\n- English Point\r\n\r\nbeberapa merk jam memiliki ukuran yang berbeda silahkan diskusi dengan kami melalu fitur form/email	2020-01-19 22:53:03.609543+07	\N	izzul	\N	C002	http://strapkulit.com/product/Abstract/18pWb3FOk5TGjCq5	18pWb3FOk5TGjCq5	18 mm	180000
\.


--
-- TOC entry 2815 (class 0 OID 16398)
-- Dependencies: 196
-- Data for Name: t_user; Type: TABLE DATA; Schema: public; Owner: strapkul
--

COPY public.t_user (id_user, password, name, role) FROM stdin;
admin	5f29fd3465c09c43401b60777dab4d00	admin	admin
test_001	2f659c236965c7d4832146ef8846251d	test	admin
test_002	2f659c236965c7d4832146ef8846251d	test2	admin
test_003	2f659c236965c7d4832146ef8846251d	test3	user
test_004	2f659c236965c7d4832146ef8846251d	test4	user
test_005	1d75028ee3e85e6ff48a565001237ef9	test05	admin
	2d02e669731cbade6a64b58d602cf2a4	sssss	user
null	7815696ecbf1c96e6894b779456d330e	asd	user
as	2d02e669731cbade6a64b58d602cf2a4	sssss	user
xdfasda	a8f5f167f44f4964e6c998dee827110c	aaa aaa	aaa aaa
tss	2d02e669731cbade6a64b58d602cf2a4	sasa	admin
izzul	f2b3f3b6b908e6c9d7d3e9ba3300cee2	izzul	admin
userizzul	63b3a06fe53720cd53f01dcdc722d7f8	izzul	user
\.


--
-- TOC entry 2685 (class 2606 OID 16413)
-- Name: t_category t_category_pkey; Type: CONSTRAINT; Schema: public; Owner: strapkul
--

ALTER TABLE ONLY public.t_category
    ADD CONSTRAINT t_category_pkey PRIMARY KEY (id_category);


--
-- TOC entry 2691 (class 2606 OID 24587)
-- Name: t_images t_images_pkey; Type: CONSTRAINT; Schema: public; Owner: strapkul
--

ALTER TABLE ONLY public.t_images
    ADD CONSTRAINT t_images_pkey PRIMARY KEY (id_image);


--
-- TOC entry 2688 (class 2606 OID 24585)
-- Name: t_product t_product_pkey; Type: CONSTRAINT; Schema: public; Owner: strapkul
--

ALTER TABLE ONLY public.t_product
    ADD CONSTRAINT t_product_pkey PRIMARY KEY (id_product);


--
-- TOC entry 2683 (class 2606 OID 16402)
-- Name: t_user t_user_pkey; Type: CONSTRAINT; Schema: public; Owner: strapkul
--

ALTER TABLE ONLY public.t_user
    ADD CONSTRAINT t_user_pkey PRIMARY KEY (id_user);


--
-- TOC entry 2686 (class 1259 OID 16421)
-- Name: fki_f_category; Type: INDEX; Schema: public; Owner: strapkul
--

CREATE INDEX fki_f_category ON public.t_product USING btree (id_category);


--
-- TOC entry 2689 (class 1259 OID 24593)
-- Name: fki_id_product; Type: INDEX; Schema: public; Owner: strapkul
--

CREATE INDEX fki_id_product ON public.t_images USING btree (id_product);


--
-- TOC entry 2693 (class 2606 OID 24588)
-- Name: t_images id_product; Type: FK CONSTRAINT; Schema: public; Owner: strapkul
--

ALTER TABLE ONLY public.t_images
    ADD CONSTRAINT id_product FOREIGN KEY (id_product) REFERENCES public.t_product(id_product);


--
-- TOC entry 2692 (class 2606 OID 24594)
-- Name: t_product t_product_id_category_fkey; Type: FK CONSTRAINT; Schema: public; Owner: strapkul
--

ALTER TABLE ONLY public.t_product
    ADD CONSTRAINT t_product_id_category_fkey FOREIGN KEY (id_category) REFERENCES public.t_category(id_category);


-- Completed on 2020-01-21 00:02:52

--
-- PostgreSQL database dump complete
--

