var express = require('express');
var router = express.Router();

// Mail Handler Function
var nodemailer = require('nodemailer');

var smtpConfig = {
  host: 'mail.strapkulit.com',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'info@strapkulit.com',
    pass: 'sem@ngat'
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
};


var session = require('express-session');
router.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));

var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

// Database
var db = require('./query');

// Static Dir
var path = __dirname + '/html/';
var pathImg = __dirname + '/public/img';

// Location Images Uploaded
var multer = require('multer');
var Storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, pathImg);
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
  }
});
var upload = multer({ storage: Storage })


router.get("/", function (req, res) {
  var category_name = [];
  var category_url = [];

  db.getCategory(function (category) {
    // Parsing Json Object
    var objects = JSON.parse(category);
    console.log(objects)
    objects.forEach(function (key, index) {
      category_name.push(key.name);
      category_url.push(key.url_param);
    });
    res.render('home', {
      category_name: category_name,
      category_url: category_url
    });

  });

  // res.sendFile(path + "index.html");
  // res.sendFile(path + "template.html");


});

router.get("/home", function (req, res) {
  var category_name = [];
  var category_url = [];

  db.getCategory(function (category) {
    // Parsing Json Object
    var objects = JSON.parse(category);
    console.log(objects)
    objects.forEach(function (key, index) {
      category_name.push(key.name);
      category_url.push(key.url_param);
    });
    res.render('home', {
      category_name: category_name,
      category_url: category_url
    });

  });


  //res.sendFile(path + "index.html");
  // res.sendFile(path + "template.html");

});


router.get("/home/auth", function (req, res) {
  res.sendFile(path + "login.html");
});

router.post('/home/auth', function (request, response) {
  var username = request.body.username;
  var password = request.body.password;
  console.log("Username : " + username);
  console.log("Password : " + password);

  db.getUserByUserId(request, function (user_value) {
    var id_user;
    var role;

    // Parsing Json Object
    var objects = JSON.parse(user_value);
    console.log(objects)
    objects.forEach(function (key, index) {
      id_user = key.id_user;
      role = key.role;
    });

    if (id_user == username && role == "admin") {
      request.session.loggedin = true;
      request.session.username = username;
      response.redirect('/home/choose-action');
    } else {
      response.sendFile(path + "404.html")
    }
  })
});

router.get("/home/choose-action", function (request, response) {
  if (request.session.loggedin) {
    response.render('choose_action', {
      username: request.session.username
    });
  } else {
    response.send('Please login to view this page!');
  }
});

router.post("/home/choose-action", function (request, response) {
  if (request.session.loggedin) {
    console.log(request.body)
    if (request.body.action_form == 'create_category') {
      response.redirect('/home/create-category');
    } else if (request.body.action_form == 'input_product') {
      response.redirect('/home/input-product');
    } else {
      response.send("No Action")
    }
  } else {
    response.send('Please login to view this page!');
  }
});


router.get("/home/input-product", function (request, response) {
  if (request.session.loggedin) {
    // Get Category List
    db.getCategory(function (category_list) {
      var category_name = [];
      var category_id = [];

      var objects = JSON.parse(category_list);
      objects.forEach(function (key, index) {
        category_name.push(key.name);
        category_id.push(key.id_category);
      });
      console.log(category_name);
      response.render('input_product', {
        category_name: category_name,
        category_id: category_id,
        username: request.session.username
      });
    });

  } else {
    response.send('Please login to view this page!');
  }
});

router.get("/home/create-category", function (request, response) {
  if (request.session.loggedin) {
    // Get Category List
    db.getCategory(function (category_list) {
      var category_name = [];
      var category_id = [];

      var objects = JSON.parse(category_list);
      objects.forEach(function (key, index) {
        category_name.push(key.name);
        category_id.push(key.id_category);
      });
      console.log(category_name);
      response.render('create_category', {
        category_name: category_name,
        category_id: category_id,
        username: request.session.username
      });
    });

  } else {
    response.send('Please login to view this page!');
  }
});

// Handler Mail Form
// router.post('/home/mail-handler')
router.post('/home/mailhandler', function (request, response) {

  console.log("From Form : " + request.body.name);
  console.log("From Form : " + request.body.email);
  console.log("From Form : " + request.body.phone);
  console.log("From Form : " + request.body.message);

  var transporter = nodemailer.createTransport(smtpConfig);
  var mailOptions = {
    from: request.body.email,
    to: 'info@strapkulit.com',
    subject: 'Mail From Form Website - Strapkulit.com',
    text: 'Name : ' + request.body.name + '\nMessage : ' + request.body.message + '\nPhone Number : ' + request.body.phone
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });

  response.redirect('/');
});

router.post('/home/input-product', upload.array('imgStraps'), (request, response, next) => {
  // Can Handle Upload File Image
  // Can Handle Get Category

  // Create Product ID
  // Generate id based on Category
  var urlPrm = "";
  var id_product = "";
  id_product = db.makeid(16);

  // Get url-parameter on Based On Category ID
  try {
    db.getCategoryById(request.body.category, function (retCategory) {
      var objects = JSON.parse(retCategory);
      objects.forEach(function (key, index) {
        urlPrm = key.url_param;
      });
      console.log("Body : " + JSON.stringify(request.body));

      // Add New Object Value to JSON Object (id_product and product Link)
      // Create Product ID Based On Category
      request.body.product['id'] = id_product;

      // Create Link Product
      request.body.product['link'] = ("http://strapkulit.com/product/" + urlPrm + "/" + id_product);
      console.log("Body : " + JSON.stringify(request.body));

      // Insert To Product Table
      db.addProduct(request, function (p_value) {

        // Get Product ID and Add Image to Table By ID Product
        var objects = JSON.parse(p_value);
        objects.forEach(function (key, index) {
          id_product = key.id_product;
        });
        console.log("ID PRODUK : " + id_product)

        // Save Image To Path
        const files = request.files;
        if (!files) {
          const error = new Error('Please upload a file')
          error.httpStatusCode = 400
          return next(error)
        }

        // Input Request For Insert Data To Table Image
        var obj_request = {};
        obj_request['body'] = {}
        obj_request.body['id_product'] = id_product;
        obj_request.body['username'] = request.body.username;
        obj_request.body['image'] = {}

        var objects = JSON.parse(JSON.stringify(files))

        objects.forEach(function (key, index) {
          // Get Image Name On Image Directory From Uploaded Files
          console.log(index + key.filename)
          obj_request.body.image['location'] = '/public/img/' + key.filename;
          obj_request.body.image['name'] = key.filename;
          obj_request.body.image['id'] = db.makeid(32);

          //console.log(JSON.stringify(obj_request));
          // Inset to Table Image
          db.addImages(obj_request, function (i_value) {

          });

        });
      });
    });
    // Success Input Data
    response.redirect('/home/input-product');
  } catch (err) {
    // Error Input Data
    console.log(err);
    response.redirect('/home/input-product');
  }
});


router.post('/home/create-category', function (request, response) {
  // Can Handle Upload File Image
  // Can Handle Get Category

  // Create Product ID
  // Generate id based on Category
  var urlPrm = "";
  var id_product = "";
  id_product = db.makeid(16);

  // Get url-parameter on Based On Category ID
  try {
    console.log("Body : " + request.body.category.id);
    console.log("Body : " + request.body.category.name);
    if (request.body.category.name.indexOf(' ') !== -1) {
      urlPrm = request.body.category.name.split(' ').join('-')
      request.body.category["url_param"] = urlPrm
    } else {
      console.log("no space")
      urlPrm = request.body.category.name
      request.body.category["url_param"] = urlPrm
    }


    console.log("Body : " + JSON.stringify(request.body.category));


    // Input to DB
    db.createCategory(request, function (c_value) {
      console.log(c_value);
    });

    // Success Input Data
    response.redirect('/home/create-category');
  } catch (err) {
    // Error Input Data
    console.log(err);
    response.redirect('/home/create-category');
  }
});



//export this router to use in our index.js
module.exports = router;