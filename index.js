var express = require("express")
var app = express();

var parse_path = require('path');
var fs = require('fs')

// Static Dir
var path = __dirname + '/html/';
var img_path = __dirname + '/public/img/';
var assets_path = __dirname + '/public/assets/';

var routerhome = require('./home');
var routerproduct = require('./product');
var routercontact = require('./contact');

// Set Templating Of Page
app.set('view engine', 'pug');
app.set('views','./views');

// Middleware Checking
app.use(function (req, res, next) {
    console.log("checking request"+"/" + req.method);
    res.setHeader('Content-Type', 'text/html');
    res.setHeader('Accept-Encoding','gzip')
    next();
});

// Route handler
app.use("/", routerhome);
app.get("/home/auth",routerhome);
app.get("/home/input-product",routerhome);


app.get("/product", routerproduct);
app.get("/product/:category", routerproduct);
app.get("/product/:category/:id", routerproduct);

app.get("/contact", routercontact);

// Routing Handler Image Load
app.use("/public", function (req, res) {
    console.log(req.url);
    var data = parse_path.parse(req.url);
    if (data.dir == "/img") {
        var img_data = data.base;
        console.log(img_data);
  
        // Get Image Base On HTML Req
        try {
            if (fs.existsSync(parse_path.join(img_path, img_data))) {
                res.sendFile(img_path + img_data);
            }
        } catch (err) {
            console.error(err);
            res.sendFile(path + "404.html");
        }
  
    } else if (data.dir == "/assets") {
        var img_data = data.base;
        console.log(img_data);
  
        // Get Image Base On HTML Req
        try {
            if (fs.existsSync(parse_path.join(assets_path, img_data))) {
                res.sendFile(assets_path + img_data);
            }
        } catch (err) {
            console.error(err);
            res.sendFile(path + "404.html");
        }
  
    } else {
        res.sendFile(path + "404.html");
    }
  
  })


app.use("*", function (req, res) {
    res.sendFile(path + "404.html");
});

app.listen(3000, function () {
    console.log("Live at Port 3000");
    
})
