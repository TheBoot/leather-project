// Database Access
const Pool = require('pg').Pool
const pool = new Pool({
    user: 'strapkul',
    host: 'localhost',
    database: 'strapkul_db_v1',
    password: 'sem@ngat',
    port: 5432,
})

function getUsers(res) {
    pool.query('SELECT * FROM t_user ORDER BY id_user ASC', (error, results) => {
        if (error) {
            console.log("Error: No Data")
            throw error
        }

        return res(JSON.stringify(results.rows))
    })
}

function getUserByUserId(req, res) {
    pool.query('SELECT * FROM t_user WHERE id_user = $1 AND password = md5($2)', [req.body.username, req.body.password], (error, results) => {
        if (error) {
            console.log("Error: No Data")
            throw error
        }
        return res(JSON.stringify(results.rows))
    })

}

function getCategory(res) {
    pool.query('SELECT * FROM t_category ORDER BY id_category ASC', (error, results) => {
        if (error) {
            console.log("Error: No Data")
            throw error
        }

        return res(JSON.stringify(results.rows))
    })
}

function getCategoryByUrlParam(req) {
    return new Promise(resolve => {
        pool.query('SELECT * FROM t_category WHERE url_param = $1', [req], (error, results) => {
            if (error) {
                console.log("Error: No Data")
                throw error
            }
            resolve(JSON.stringify(results.rows))
        })

    });
}

function getCategoryById(req, res) {

    pool.query('SELECT * FROM t_category WHERE id_category = $1', [req], (error, results) => {
        if (error) {
            console.log("Error: No Data")
            throw error
        }
        return res(JSON.stringify(results.rows))
    })
}

function getProductByCategoryId(req) {
    return new Promise(resolve => {
        pool.query('SELECT * FROM t_product WHERE id_category = $1', [req], (error, results) => {
            if (error) {
                console.log("Error: No Data")
                throw error
            }
            resolve(JSON.stringify(results.rows))
        })

    });
}

function getProductByProductId(req, res) {
    return new Promise(resolve => {
        pool.query('SELECT * FROM t_product WHERE id_product = $1', [req], (error, results) => {
            if (error) {
                console.log("Error: No Data")
                throw error
            }

            resolve(JSON.stringify(results.rows))
        })

    });
}

function getImageByProductId(req, res) {
    return new Promise(resolve => {
        pool.query('SELECT * FROM t_images WHERE id_product = $1', [req], (error, results) => {
            if (error) {
                console.log("Error: No Data")
                throw error
            }

            resolve(JSON.stringify(results.rows))
        })

    });

}

function getImageByImageId(req, res) {

    pool.query('SELECT * FROM t_images WHERE id_image = $1', [req], (error, results) => {
        if (error) {
            console.log("Error: No Data")
            throw error
        }

        return res(JSON.stringify(results.rows))
    })
}

function createImageByImageId(req, res) {

    pool.query('SELECT * FROM t_images WHERE id_image = $1', [req], (error, results) => {
        if (error) {
            console.log("Error: No Data")
            throw error
        }

        return res(JSON.stringify(results.rows))
    })
}


function createUsers(req, res) {
    pool.query('insert into t_user (id_user, password, name, role) values($1,md5($2),$3,$4)', [req.body.user.id_user,
    req.body.user.password, req.body.user.name, req.body.user.role], (error, results) => {
        if (error) {
            console.log("Error: " + error.message)
        }
        else {
            pool.query('SELECT * FROM t_user ORDER BY id_user ASC', (error, results) => {
                if (error) {
                    console.log("Error: " + error.message)
                    throw error
                } else {
                    return res(JSON.stringify(results.rows))
                }
            })
        }
    })
}

function createCategory(req, res) {
    pool.query('insert into t_category (id_category, name, url_param) values($1,$2,$3)',
        [req.body.category.id, req.body.category.name, req.body.category.url_param], (error, results) => {
            if (error) {
                console.log("Error: " + error.message)
                throw error
            }
            else {
                pool.query('SELECT * FROM t_category WHERE id_category=$1', [req.body.category.id], (error, results) => {
                    if (error) {
                        console.log("Error: " + error.message)
                        throw error
                    } else {
                        return res(JSON.stringify(results.rows))
                    }
                })
            }
        })
}


function addProduct(req, res) {
    pool.query('insert into t_product (id_product, name, description, link_product, created_date, created_by, id_category, size, price) values($1,$2,$3,$4,current_timestamp,$5,$6,$7,$8)',
        [req.body.product.id, req.body.product.name, req.body.product.description, req.body.product.link, req.body.username, req.body.category, req.body.product.size, req.body.product.price], (error, results) => {
            if (error) {
                console.log("Error: " + error.message)
                throw error
            }
            else {
                pool.query('SELECT * FROM t_product WHERE id_product=$1', [req.body.product.id], (error, results) => {
                    if (error) {
                        console.log("Error: " + error.message)
                        throw error
                    } else {
                        return res(JSON.stringify(results.rows))
                    }
                })
            }
        })
}

function addImages(req, res) {
    pool.query('insert into t_images (id_product, id_image, name, location, created_date, created_by) values($1,$2,$3,$4,current_timestamp,$5)',
        [req.body.id_product, req.body.image.id, req.body.image.name, req.body.image.location, req.body.username], (error, results) => {
            if (error) {
                console.log("Error: " + error.message)
                throw new Error(error.message)
                // return res(JSON.stringify(msg_error(error)));
            }
            else {
                pool.query('SELECT * FROM t_images WHERE id_image=$1', [req.body.image.id], (error, results) => {
                    if (error) {
                        console.log("Error: " + error.message)
                        throw new Error(error.message);
                    } else {
                        return res(JSON.stringify(results.rows))
                    }
                })
            }
        })
}

function msg_error(error) {
    var errObjects = {};
    errObjects['body'] = {};
    errObjects.body["state"] = "Error";
    errObjects.body["error_msg"] = error.message;
    return errObjects;
}

function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


module.exports = {
    getUsers,
    getUserByUserId,
    getCategory,
    getCategoryById,
    getCategoryByUrlParam,
    getImageByImageId,
    getImageByProductId,
    getProductByCategoryId,
    getProductByProductId,
    makeid,
    createCategory,
    addProduct,
    addImages
}
