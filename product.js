var express = require('express');
var fx = require("money");
var request_api = require('request');

var router = express.Router();
var app = express();

// File handler
var fs = require('fs')
var parse_path = require('path');

// Static File
var base_path = __dirname + '/html/';
var img_path = __dirname + '/public/img/';
var tmp_img_path = __dirname + '/tmp/';


// Database
var db = require('./query');

function getIDRcurrency() {
    // Return new promise 
    return new Promise(function (resolve, reject) {
        // Do async job
        request_api('https://www.freeforexapi.com/api/live?pairs=USDIDR', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                // console.log(body) // Print the google web page.
                resolve(JSON.parse(body));
            } else {
                reject(error);
            }
        })
    })

}

router.get("/product", function (req, res) {
    console.log("get product");


    var array_category_name = [];
    var array_category_url = [];
    var array_category_id = [];

    // Image Variable
    var location_img = [];

    db.getCategory(function (category) {
        // Parsing Json Object
        var objects = JSON.parse(category);
        console.log(objects)
        objects.forEach(function (key, index) {
            array_category_id.push(key.id_category);
            array_category_name.push(key.name);
            array_category_url.push(key.url_param);
        });

        async function GetProductData() {
            // Product Variable
            var product_id = [];
            var product_name = [];

            for (var i = 0; i < array_category_id.length; i++) {
                console.log("CATEGORY_ID : " + array_category_id[i])
                // Get Product ID
                const product = await db.getProductByCategoryId(array_category_id[i]);
                var o_product = JSON.parse(product);
                var tmp_product_id;
                o_product.forEach(function (key, index) {
                    tmp_product_id = o_product[0].id_product;
                });
                product_id.push(tmp_product_id);
            }

            for (var i = 0; i < product_id.length; i++) {
                console.log("PRODUCT_ID : " + product_id[i])
                var tmp_location_img;
                const images = await db.getImageByProductId(product_id[i]);
                var o_images = JSON.parse(images);
                o_images.forEach(function (key, index) {
                    tmp_location_img = o_images[0].location;
                });
                location_img.push(tmp_location_img);
            }
            console.log("IMAGES: " + location_img);

            res.render('product', {
                all_category_name: array_category_name,
                all_category_url: array_category_url,
                location_img: location_img
            });

        }
        GetProductData();
    });


});


router.get('/product/:category', function (req, res) {

    console.log("category : " + req.params.category);

    // Get All Category
    var array_category_name = [];
    var array_category_url = [];

    db.getCategory(function (category) {
        // Parsing Json Object
        var objects = JSON.parse(category);
        console.log(objects)
        objects.forEach(function (key, index) {
            array_category_name.push(key.name);
            array_category_url.push(key.url_param);
        });
    });

    // Get Product by category
    async function GetProductData() {
        // Category Variable
        var category_id;
        var category_name;
        var url_param;
        // Product Variable
        var product_id = [];
        var product_name = [];
        // Image Variable
        var location_img = [];

        const category = await db.getCategoryByUrlParam(req.params.category);
        var o_category = JSON.parse(category);
        o_category.forEach(function (key, index) {
            category_id = key.id_category;
            category_name = key.name;
            url_param = key.url_param;
        });

        const product = await db.getProductByCategoryId(category_id);
        var o_product = JSON.parse(product);
        o_product.forEach(function (key, index) {
            product_id.push(key.id_product);
            product_name.push(key.name);
        });

        for (var i = 0; i < product_id.length; i++) {
            var tmp_location_img;
            const images = await db.getImageByProductId(product_id[i]);
            var o_images = JSON.parse(images);
            o_images.forEach(function (key, index) {
                tmp_location_img = key.location;
            });
            location_img.push(tmp_location_img);
        }

        console.log("Category ID : " + category_id)
        console.log("Category Name : " + category_name)
        console.log("Product Id : " + product_id)
        console.log("Product Name : " + product_name)

        console.log("Product Image Location : " + location_img)

        res.render('product_by_category', {
            category_url: url_param,
            category_name: category_name,
            product_id: product_id,
            product_name: product_name,
            location_img: location_img,
            all_category_name: array_category_name,
            all_category_url: array_category_url
        });
    }
    // Asyncronus Function Get Product By Category ID
    GetProductData(); // Render Data Product By Category Id
});

router.get('/product/:category/:id', function (req, res) {
    // Default Currency

    async function getProductDatabyID() {
        var product_id;
        var product_name;
        var product_description;
        var product_price;
        var product_price_usd;
        var product_size;
        var location_img = [];

        // Category Variable
        var category_id;
        var category_name;
        var url_param;

        // Get Category
        const category = await db.getCategoryByUrlParam(req.params.category);
        var o_category = JSON.parse(category);
        o_category.forEach(function (key, index) {
            category_id = key.id_category;
            category_name = key.name;
            url_param = key.url_param;
        });


        // Checking ID Product on Database
        console.log("id produk : " + req.params.id);
        // Get Product 
        const product = await db.getProductByProductId(req.params.id);
        var o_product = JSON.parse(product);
        o_product.forEach(function (key, index) {
            product_id = key.id_product;
            product_name = key.name;
            product_description = key.description;
            product_size = key.size;
            product_price = key.price;
        });

        // change new line to <br>
        let product_description_tag = product_description.replace(/(?:\r\n|\r|\n)/g, '<br>');

        // Get Image location
        const images = await db.getImageByProductId(product_id);
        var o_images = JSON.parse(images);
        o_images.forEach(function (key, index) {
            location_img.push(key.location);
        });
        console.log(product_description)
        console.log(location_img)

        // Get excange rate IDR to USD
        try {
            const o_currency = await getIDRcurrency();
            // console.log(o_currency)
            fx.base = "USD";
            fx.rates = {
                "IDR": o_currency.rates.USDIDR.rate, // eg. 1 USD === Latest IDR
                "USD": 1,        // always include the base rate (1:1)
                /* etc 
                */
            }
        } catch (error) {
            // console.log(o_currency)
            fx.base = "USD";
            fx.rates = {
                "IDR": 14000, // eg. 1 USD === 14000 IDR
                "USD": 1,        // always include the base rate (1:1)
                /* etc 
                */
            }
        }

        const formatter_idr = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'IDR',
            minimumFractionDigits: 2
        })

        const formatter_usd = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2
        })

        product_price_usd = formatter_usd.format(fx(product_price).convert({ from: "IDR", to: "USD" }));
        product_price = formatter_idr.format(product_price);

        res.render('product_by_id', {
            category_url: url_param,
            category_name: category_name,
            category_url: req.params.category,
            product_id: product_id,
            product_name: product_name,
            product_description: product_description_tag,
            location_img: location_img,
            product_price: product_price,
            product_price_usd: product_price_usd,
            product_size: product_size
        });
    }

    // Async Function
    getProductDatabyID();

});



//export this router to use in our index.js
module.exports = router;