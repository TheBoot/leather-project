var express = require('express');
var router = express.Router();

// Static Dir
var path = __dirname + '/html/';

// Database
var db = require('./query');

router.get("/contact", function (req, res) {

    var category_name = [];
    var category_url = [];

    db.getCategory(function (category) {
        // Parsing Json Object
        var objects = JSON.parse(category);
        console.log(objects)
        objects.forEach(function (key, index) {
            category_name.push(key.name);
            category_url.push(key.url_param);
        });
        res.render('contact', {
            category_name: category_name,
            category_url: category_url
        });

    });

    //res.sendFile(path + "kontak.html");
});



//export this router to use in our index.js
module.exports = router;